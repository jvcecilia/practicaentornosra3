package principal;

import java.util.Scanner;

/**
 * @author Javier Cecilia
 * @since 21-02-2018
 * @version 1.1
 * 
 * Clase cuya función es la creación de contraseñas de forma aleatoria.
 * 
 */

public class Generador {


	/**
	 * Metodo que contiene la clase Scanner para introducir por teclado
	 * la longitud y el tipo de la contraseña que será generada.
	 * 
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		menu();
		
		System.out.println("Introduce la longitud de la cadena: ");
		int longitud = scanner.nextInt();
		System.out.println("Elige tipo de password: ");
		int opcion = scanner.nextInt();
		
		crearContraseñas(longitud, opcion);
		
		scanner.close();
	}

	
	/**
	 * Metodo que generara una contraseña, la cual, puede ser de 4 tipos diferentes.
	 * 
	 * @param longitud int que marca la cantidad de los caracteres de la contraseña 
	 * @param opcion int que marca el tipo de contraseña que sera generada
	 * @param password String que al final mostrara la contraseña generada.
	 */
	static void crearContraseñas(int longitud, int opcion) {
		String password = "";
		for (int i = 0; i < longitud; i++) {
		switch (opcion) {
		case 1:
			password += obtenerLetrasAleatorias();
			break;
		case 2:
			password += obtenerNumerosAleatorios();
			break;
		case 3:
			int n;
			n = (int) (Math.random() * 2);
			if (n == 1) {
				password += obtenerLetrasAleatorias();
			} else {
				password += obtenerAleatorioEspecial();
			}
			break;
		case 4:
			int n2;
			n2 = (int) (Math.random() * 3);
			if (n2 == 1) {
				password += obtenerLetrasAleatorias();
			} else if (n2 == 2) {
				password += obtenerAleatorioEspecial();
			} else {
				password += obtenerNumerosAleatorios();
			}
			break;
		}
		}
		System.out.println(password);
	}

	/**
	 * Metodo que muestra el menu del programa
	 */
	static void menu() {
		System.out.println("Programa que genera passwords de la longitud indicada, y del rango de caracteres");
		System.out.println("1 - Caracteres desde A - Z");
		System.out.println("2 - Numeros del 0 al 9");
		System.out.println("3 - Letras y caracteres especiales");
		System.out.println("4 - Letras, numeros y caracteres especiales");
	}
	
	/**
	 * 
	 * @return char entre A-Z
	 */
	static char obtenerLetrasAleatorias() {
		char letra = (char) ((Math.random() * 26) + 65);
		return letra;
	}

	/**
	 * 
	 * @return int entre 1-9
	 */
	static int obtenerNumerosAleatorios() {
		int numero = (int) (Math.random() * 10);
		return numero;
	}
	
	/**
	 * 
	 * @return char especial
	 */
	static char obtenerAleatorioEspecial() {
		char especial = (char) ((Math.random() * 15) + 33);
		return especial;
	}

	
}
