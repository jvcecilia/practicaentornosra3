package Refactorización;
/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */

import java.util.Scanner;
/**
 * 
 * @author jvcecilia
 * @since 31-01-2018
 */
public class Refactorizar2 {

	static Scanner sc = new Scanner(System.in); // Todo en la misma linea
	public static void main(String[] args) 
	{

		
		int cantidad_maxima_alumnos = 10; // Darle el valor en la misma linea que es creada
		int[] notas = new int[10]; // Corchetes al lado del tipo de variable
		for(int n = 0; n < cantidad_maxima_alumnos; n++) // Separar codigo para que sea legible
		{
			System.out.println("Introduce nota media de alumno");
			notas[n] = sc.nextInt();
		}	
		
		System.out.println("El resultado es: " + recorrer_array(notas));
		
		sc.close();
	}
	static double recorrer_array(int[] vector) // Corchetes al lado del tipo de variable
	{
		double acumulador = 0; //Cambiar el nombre de la variable para entender su funcino
		for(int i = 0; i < 10; i++) //Mejorar legibilidad
		{
			acumulador += vector[i]; //Ahorrar codigo poniendo +=
		}
		return acumulador/10;
	}
	
}